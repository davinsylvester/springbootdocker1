package com.davin.docker.dockerspringboot.resource;

import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.ls.LSOutput;

@RestController
@RequestMapping("/") //http://localhost:8080/
public class HelloResource {

    @GetMapping
    public String hello(){
     return "Hello there!";
    }



}
